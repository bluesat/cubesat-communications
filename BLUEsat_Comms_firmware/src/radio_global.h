#ifndef RADIO_GLOBAL_H
#define RADIO_GLOBAL_H

#include <msp430.h>
#include "lcd_dogm128_6.h"
#include "hal_spi_rf_trxeb.h"
#include "cc112x_spi.h"
#include "stdlib.h"
#include "bsp.h"
#include "bsp_key.h"
#include "io_pin_int.h"
#include "bsp_led.h"
#include "register_data.h"


#define PACKET_LENGTH               750     // Between 256 and 2^16 (for infinite length mode)
#define FIFO_SIZE                   128
#define CRC_OK                      0x80
#define AVAILABLE_BYTES_IN_TX_FIFO  122     // # of bytes one can write to the
                                            // TX_FIFO when a falling edge occur
                                            // on IOCFGx = 0x02 and
                                            // FIFO_THR = 120
#define BYTES_IN_TX_FIFO            (FIFO_SIZE - AVAILABLE_BYTES_IN_TX_FIFO)
#define INFINITE                    0
#define FIXED                       1
#define MAX_VARIABLE_LENGTH         255
#define INFINITE_PACKET_LENGTH_MODE 0x40
#define FIXED_PACKET_LENGTH_MODE    0x00

#define GPIO3                       0x04
#define GPIO2                       0x08
#define GPIO0                       0x80

extern uint8 txBuffer[PACKET_LENGTH + 2];   // Buffer used to hold the packet
                                            // to be sent
extern uint16 packetCounter;            // Counter keeping track of
                                            // packets sent
extern uint16 packetLength; // Length byte inserted in two first
                                            // bytes of the TX FIFO
extern uint32 bytesLeft;                    // Keeping track of bytes left to
                                            // write to the TX FIFO
extern uint8 fixedPacketLength;
extern uint8 *pBufferIndex;                 // Pointer to current position in
                                            // the txBuffer
extern uint8 iterations;                    // For packets greater than 128
                                            // bytes, this variable is used to
                                            // keep track of how many time the
                                            // TX FIFO should be re-filled to
                                            // its limit
extern uint8 writeRemainingDataFlag;        // When this flag is set, the
                                            // TX FIFO should not be filled
                                            // entirely
extern uint8 pktFormat;

extern uint8 packetSent;            // Flag set when packet is sent


void waitMs(uint16 msec);
void waitUs(uint16 msec);
void packetSentISR();
void msk_send_packet( uint8_t *data, int length );
void init_radio_msk();
void txFifoBelowThresholdISR();
void manualCalibration();

#endif