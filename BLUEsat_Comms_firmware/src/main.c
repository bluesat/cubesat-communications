#include "msp430.h"
#include "lcd_dogm128_6.h"
#include "hal_spi_rf_trxeb.h"
#include "cc112x_spi.h"
#include "stdlib.h"
#include "bsp.h"
#include "bsp_key.h"
#include "io_pin_int.h"
#include "bsp_led.h"
#include "register_data.h"
#include "radio_global.h"
#include "morse.h"
#include "driverlib.h"

static void initMCU(void);
static void updateLcd(void);
static void init_uart();

uint8_t receivedDataBuffer[128];
uint8_t receivedDataIndex;

bool processing_packet = false;

void uart_send_string( char *s ) {
    for( int i = 0; s[i] != 0; ++i )
        USCI_A_UART_transmitData(USCI_A2_BASE, s[i]);
}

void main(void) {

    initMCU();
    
    init_radio_morse();
    
    init_uart();
    
    receivedDataIndex = 0;
    uint8_t prevDataIndex = 0;

    updateLcd();
    
    while( true ) {
      
        // UART packet has come in
        if( prevDataIndex != receivedDataIndex ) {
            
            // Wait for the packet to fill the receive buffer
            waitMs(100);
            
            processing_packet = true;
            
            // Add a null terminator to the receive buffer
            receivedDataBuffer[receivedDataIndex]=0;
            
            // Process it
            if( receivedDataIndex < 2 ) {
                uart_send_string("NO COMMAND");
            } else if ( receivedDataBuffer[0] != '$' ) {
                uart_send_string("BAD COMMAND");
            } else if ( receivedDataBuffer[1] == 'M' ) {
                init_radio_morse();
                send_morse_string((const char*)&receivedDataBuffer[2]);
                uart_send_string("OK");
            } else if ( receivedDataBuffer[1] == 'D' ) {
                init_radio_msk();
                msk_send_packet(&receivedDataBuffer[2], receivedDataIndex-2);
                uart_send_string("OK");
            } else {
                uart_send_string("UNKNOWN COMMAND");
            }
            
            updateLcd();
            
            // Wipe the packet
            receivedDataIndex = 0;
            
            processing_packet = false;
        }

        // Wait for button push
        //while(!bspKeyPushed(BSP_KEY_ALL));

        prevDataIndex = receivedDataIndex;
        
        waitMs(100);
    }
}

void init_uart() {
    // Set up UART
    //Stop WDT
    WDT_A_hold(WDT_A_BASE);

    //P9.4,5 = USCI_A2 TXD/RXD
    GPIO_setAsPeripheralModuleFunctionInputPin(
            GPIO_PORT_P9,
            GPIO_PIN4 + GPIO_PIN5
            );

    //Baudrate = 9600, clock freq = 8MHz
    if ( STATUS_FAIL == USCI_A_UART_initAdvance(USCI_A2_BASE,
                                                USCI_A_UART_CLOCKSOURCE_SMCLK,
                                                52,
                                                1,
                                                0,
                                                USCI_A_UART_NO_PARITY,
                                                USCI_A_UART_LSB_FIRST,
                                                USCI_A_UART_ONE_STOP_BIT,
                                                USCI_A_UART_MODE,
                                                USCI_A_UART_OVERSAMPLING_BAUDRATE_GENERATION ))
            return;

    //Enable UART module for operation
    USCI_A_UART_enable(USCI_A2_BASE);

    //Enable Receive Interrupt
    USCI_A_UART_clearInterruptFlag(USCI_A2_BASE,
                                   USCI_A_UART_RECEIVE_INTERRUPT);
    USCI_A_UART_enableInterrupt(USCI_A2_BASE,
                                USCI_A_UART_RECEIVE_INTERRUPT);

    __enable_interrupt();
}

#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector=USCI_A2_VECTOR
__interrupt
#elif defined(__GNUC__)
__attribute__((interrupt(USCI_A2_VECTOR)))
#endif
void USCI_A2_ISR(void)
{
        switch (__even_in_range(UCA2IV, 4)) {
            //Vector 2 - RXIFG
            case 2:
                if( !processing_packet ) {
                    ++receivedDataIndex;
                    receivedDataBuffer[receivedDataIndex-1] = USCI_A_UART_receiveData(USCI_A2_BASE);
                }
                break;
            default:
                break;
        }
}

static void updateLcd(void) {
    lcdBufferClear(0);
    lcdBufferPrintString(0, "BLUEsat Flatsat Comms", 0, eLcdPage0);
    lcdBufferSetHLine(0, 0, LCD_COLS - 1, eLcdPage7);
    lcdBufferPrintString(0, (char*)receivedDataBuffer, 0, eLcdPage5);
    lcdBufferPrintString(0, "# CMD:", 0, eLcdPage2);
    lcdBufferPrintInt(0, (int32)(++packetCounter), 80, eLcdPage2);
    lcdBufferPrintString(0, "Packet TX", 0, eLcdPage7);
    lcdBufferSetHLine(0, 0, LCD_COLS - 1, 55);
    lcdBufferInvertPage(0, 0, LCD_COLS, eLcdPage7);
    lcdSendBuffer(0);
}


static void initMCU(void) {

    // Init clocks and I/O
    bspInit(BSP_SYS_CLK_8MHZ);

    // Init LEDs
    bspLedInit();

    // Init buttons
    bspKeyInit(BSP_KEY_MODE_POLL);

    // Initialize SPI interface to LCD (shared with SPI flash)
    bspIoSpiInit(BSP_FLASH_LCD_SPI, BSP_FLASH_LCD_SPI_SPD);

    // Init LCD
    lcdInit();

    // Instantiate transceiver RF SPI interface to SCLK ~ 4 MHz
    // Input parameter is clockDivider
    // SCLK frequency = SMCLK/clockDivider
    trxRfSpiInterfaceInit(2);

    // Enable global interrupt
    _BIS_SR(GIE);
}


