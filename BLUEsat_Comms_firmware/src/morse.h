// Seb Holzapfel for BLUEsat UNSW, 2015

#ifndef MORSE_H
#define MORSE_H

// Switches the radio into morse mode
void init_radio_morse();

// Send a single morse character
void send_morse_symbol( const char in_char );

// Send a string; with word spacing.
void send_morse_string( const char *string );

#endif