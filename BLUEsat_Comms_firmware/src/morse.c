// Seb Holzapfel for BLUEsat UNSW, 2015

#include "morse.h"
#include "radio_global.h"
#include <ctype.h>

#define MORSE_UNIT 128 //Milliseconds

// Letters, A-Z (ASCII 65 to 90)
static const char *morse_letters[]={
    ".-","-...","-.-.","-..",".","..-.","--.","....","..",".---",
    "-.-",".-..","--","-.","---",".--.","--.-",".-.","...","-",
    "..-","...-",".--","-..-","-.--","--.."
};

// Numbers, 0-9 (ASCII 48 to 57)
static char *morse_numbers[]={
    "-----",".----","..---","...--","....-",".....","-....","--...",
    "---..","----."
};

// To store more exotic symbols
typedef struct {
    const char *codes;
    const char *value;
} morse_symbol_t;

// Punctuation & other commands
static const morse_symbol_t morse_other[]={
  {"\\/", "-..-."},  // Slash
  {":;", "---..."},  // Colon
  {"'", ".----."},   // Apostrophe
  {"-_","-....-"},   // Hyphen
  {"()", "-.--.-"},  // Parentheses
  {"\"", ".-..-."},  // Quotation marks
  {"@", ".--.-."},   // @ sign
  {",", "--..--"},   // Comma
  {".", ".-.-.-"},   // Full stop
  {"?", "..--.."},   // Question mark
  {"\n", ".-.-"},    // New line
  {"=", "-...-"},    // New paragraph or = sign
  {"+", ".-.-."},    // End of message or + sign
  {"#", "-.--."},    // Invite to transmit or # symbol
  {"$","...-.-"}     // End of transmission or $ sign
};

void registerConfigMorse() {

    uint8 writeByte;

    // Reset radio
    trxSpiCmdStrobe(CC112X_SRES);

    // Write registers to radio
    for(uint16 i = 0;
        i < (sizeof(morseSettings)/sizeof(registerSetting_t)); i++) {
        writeByte = morseSettings[i].data;
        cc112xSpiWriteReg(morseSettings[i].addr, &writeByte, 1);
    }
}

// Switches the radio into morse mode
void init_radio_morse() {
    
    // Disable interrupts
    ioPinIntDisable(IO_PIN_PORT_1, GPIO0);
    ioPinIntDisable(IO_PIN_PORT_1, GPIO2);
    
    // Clear interrupt
    ioPinIntClear(IO_PIN_PORT_1, GPIO0);
    
     // Clear interrupt
    ioPinIntClear(IO_PIN_PORT_1, GPIO2);
    
    // Reset radio, write new registers
    registerConfigMorse();
    
    uint8 writeByte;

    // Application specific registers
    // FIFO_THR = 120
    // GPIO0 = TXFIFO_THR
    // GPIO2 = PKT_SYNC_RXTX
    writeByte = INFINITE_PACKET_LENGTH_MODE;
    cc112xSpiWriteReg(CC112X_PKT_CFG0, &writeByte, 1);
    writeByte = 0x78; cc112xSpiWriteReg(CC112X_FIFO_CFG, &writeByte, 1);
    writeByte = 0x02; cc112xSpiWriteReg(CC112X_IOCFG0,   &writeByte, 1);
    writeByte = 0x06; cc112xSpiWriteReg(CC112X_IOCFG2,   &writeByte, 1);

    bspLedSet(BSP_LED_ALL);

    // Calibrate the radio according to the errata note
    manualCalibration();

    // Connect ISR function to GPIO0
    ioPinIntRegister(IO_PIN_PORT_1, GPIO0, &txFifoBelowThresholdISR);

    // Interrupt on falling edge
    ioPinIntTypeSet(IO_PIN_PORT_1, GPIO0, IO_PIN_FALLING_EDGE);

    // Enable interrupt
    ioPinIntEnable(IO_PIN_PORT_1, GPIO0);

    // Connect ISR function to GPIO2
    ioPinIntRegister(IO_PIN_PORT_1, GPIO2, &packetSentISR);

    // Interrupt on falling edge
    ioPinIntTypeSet(IO_PIN_PORT_1, GPIO2, IO_PIN_FALLING_EDGE);

    // Enable interrupt
    ioPinIntEnable(IO_PIN_PORT_1, GPIO2);

    // Create data packet
    txBuffer[0] = (uint8)(packetLength >> 8);
    txBuffer[1] = (uint8)(packetLength & 0x00FF);
    for (uint16 i = 2; i < PACKET_LENGTH + 2; i++)
        txBuffer[i] = 0xFF;
}

// Abuse infinite-packet mode to send a pulse of CW
void send_cw_pulse( uint16 length ) {
    uint8 writeByte;
    writeByte = INFINITE_PACKET_LENGTH_MODE;
    cc112xSpiWriteReg(CC112X_PKT_CFG0, &writeByte, 1);
    pktFormat = INFINITE;

    // Update variables
    writeRemainingDataFlag = FALSE;
    bytesLeft = length + 2;
    pBufferIndex = txBuffer + FIFO_SIZE;
    fixedPacketLength = bytesLeft % (MAX_VARIABLE_LENGTH + 1);

    // Configure the PKT_LEN register
    writeByte = fixedPacketLength;
    cc112xSpiWriteReg(CC112X_PKT_LEN, &writeByte, 1);

    // Fill up the TX FIFO
    cc112xSpiWriteTxFifo(txBuffer, FIFO_SIZE);

    bytesLeft -= FIFO_SIZE;

    iterations = (bytesLeft / AVAILABLE_BYTES_IN_TX_FIFO);

    // Enter TX mode
    trxSpiCmdStrobe(CC112X_STX);

    // Clear isr flag on GPIO0
    ioPinIntClear(IO_PIN_PORT_1, GPIO0);

    // Enable interrupt on GPIO0
    ioPinIntEnable(IO_PIN_PORT_1, GPIO0);

    // Wait for packet to be sent
    while (!packetSent);
    packetSent = FALSE;
}

// 1 unit on, 1 unit off
void dit() {
    send_cw_pulse( 256 );
    waitMs(MORSE_UNIT);
}

// 3 units on, 1 unit off
void dah() {
    send_cw_pulse( 768 );
    waitMs(MORSE_UNIT);
}

// 3-1 units off (because of previous dit or dah)
void symbol_halt() {
    waitMs(MORSE_UNIT*2);
}

// 7-1 units off
void word_halt() {
    waitMs(MORSE_UNIT*6);
}

// Send a string that looks like "--.--..."
void send_morse( const char *s ) {
    while( true ) {
        if (*s == 0) return;
        *s == '.' ? dit() : dah();
        ++s;
    }
}

// Send a single morse character
void send_morse_symbol( const char in_char ) {
    // Lowercase is treated as uppercase
    char c = toupper(in_char);
    
    if( c >= 'A' && c <= 'Z' ) {
      
        send_morse( morse_letters[c - 'A'] );
        
    } else if ( c >= '0' && c <= '9' ) {
      
        send_morse( morse_numbers[c - '0'] );
        
    } else {
      
        // Find a corresponding morse code in special symbols
        bool sent = false;
        for( uint8 i = 0; i != sizeof(morse_other)/sizeof(morse_symbol_t)
             && !sent; ++i ) {
              
            const char *s = morse_other[i].codes;
            while( true ) {
                if( *s == 0 ) break;
                if( *s == c ) {
                    send_morse( morse_other[i].value );
                    sent = true;
                    break;
                }
                ++s;
            }
          
        }
        
        if(!sent) {
            //Unknown symbol? Send question mark
            send_morse_symbol('?');
        }
    }
    
    symbol_halt();
}

// Send a string; with word spacing.
void send_morse_string( const char *s ) {
    while( true ) {
        if( *s == 0 ) break;
        if( *s != ' ' ) {
            send_morse_symbol( *s );
        } else {
            word_halt();
        }
        ++s;
    }
}