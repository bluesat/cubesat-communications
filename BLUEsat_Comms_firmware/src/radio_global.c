#include "radio_global.h"

uint8 txBuffer[PACKET_LENGTH + 2];   // Buffer used to hold the packet
                                     // to be sent
uint16 packetCounter = 0;            // Counter keeping track of
                                     // packets sent
uint16 packetLength = PACKET_LENGTH; // Length byte inserted in two first
                                     // bytes of the TX FIFO
uint32 bytesLeft;                    // Keeping track of bytes left to
                                     // write to the TX FIFO
uint8 fixedPacketLength;
uint8 *pBufferIndex;                 // Pointer to current position in
                                     // the txBuffer
uint8 iterations;                    // For packets greater than 128
                                     // bytes, this variable is used to
                                     // keep track of how many time the
                                     // TX FIFO should be re-filled to
                                     // its limit
uint8 writeRemainingDataFlag;        // When this flag is set, the
                                            // TX FIFO should not be filled
                                            // entirely
uint8 pktFormat = INFINITE;
uint8 packetSent = FALSE;


void packetSentISR() {

    packetSent = TRUE;

    // Clear ISR flag
    ioPinIntClear(IO_PIN_PORT_1, GPIO2);
}


void txFifoBelowThresholdISR() {

    uint8 writeByte;

    if (writeRemainingDataFlag) { // Less than 120 bytes to write to the TX FIFO

        cc112xSpiWriteTxFifo(pBufferIndex, bytesLeft);  // Write remaining bytes
                                                        // to the TX FIFO
        // Disable interrupt on GPIO0
        ioPinIntDisable(IO_PIN_PORT_1, GPIO0);

    } else { // Fill up the TX FIFO
        cc112xSpiWriteTxFifo(pBufferIndex, AVAILABLE_BYTES_IN_TX_FIFO);

        // Change to fixed packet length mode when there is less than 256 bytes
        // left to transmit
        if ((bytesLeft < (MAX_VARIABLE_LENGTH + 1 - BYTES_IN_TX_FIFO))
            && (pktFormat == INFINITE)) {

            writeByte = FIXED_PACKET_LENGTH_MODE;
            cc112xSpiWriteReg(CC112X_PKT_CFG0, &writeByte, 1);
            pktFormat = FIXED;
        }

        // Update the variables keeping track of how many more bytes should be
        // written to the TX FIFO and where in txBuffer data should 
        // be taken from
        pBufferIndex += AVAILABLE_BYTES_IN_TX_FIFO;
       
        bytesLeft -= AVAILABLE_BYTES_IN_TX_FIFO;
        
        if (!(--iterations))
            writeRemainingDataFlag = TRUE;
    }

    // Clear ISR flag
    ioPinIntClear(IO_PIN_PORT_1, GPIO0);
}

#define VCDAC_START_OFFSET 2
#define FS_VCO2_INDEX 0
#define FS_VCO4_INDEX 1
#define FS_CHP_INDEX 2
void manualCalibration() {

    uint8 original_fs_cal2;
    uint8 calResults_for_vcdac_start_high[3];
    uint8 calResults_for_vcdac_start_mid[3];
    uint8 marcstate;
    uint8 writeByte;

    // 1) Set VCO cap-array to 0 (FS_VCO2 = 0x00)
    writeByte = 0x00;
    cc112xSpiWriteReg(CC112X_FS_VCO2, &writeByte, 1);

    // 2) Start with high VCDAC (original VCDAC_START + 2):
    cc112xSpiReadReg(CC112X_FS_CAL2, &original_fs_cal2, 1);
    writeByte = original_fs_cal2 + VCDAC_START_OFFSET;
    cc112xSpiWriteReg(CC112X_FS_CAL2, &writeByte, 1);

    // 3) Calibrate and wait for calibration to be done
    //   (radio back in IDLE state)
    trxSpiCmdStrobe(CC112X_SCAL);

    do {
        cc112xSpiReadReg(CC112X_MARCSTATE, &marcstate, 1);
    } while (marcstate != 0x41);

    // 4) Read FS_VCO2, FS_VCO4 and FS_CHP register obtained with
    //    high VCDAC_START value
    cc112xSpiReadReg(CC112X_FS_VCO2,
                     &calResults_for_vcdac_start_high[FS_VCO2_INDEX], 1);
    cc112xSpiReadReg(CC112X_FS_VCO4,
                     &calResults_for_vcdac_start_high[FS_VCO4_INDEX], 1);
    cc112xSpiReadReg(CC112X_FS_CHP,
                     &calResults_for_vcdac_start_high[FS_CHP_INDEX], 1);

    // 5) Set VCO cap-array to 0 (FS_VCO2 = 0x00)
    writeByte = 0x00;
    cc112xSpiWriteReg(CC112X_FS_VCO2, &writeByte, 1);

    // 6) Continue with mid VCDAC (original VCDAC_START):
    writeByte = original_fs_cal2;
    cc112xSpiWriteReg(CC112X_FS_CAL2, &writeByte, 1);

    // 7) Calibrate and wait for calibration to be done
    //   (radio back in IDLE state)
    trxSpiCmdStrobe(CC112X_SCAL);

    do {
        cc112xSpiReadReg(CC112X_MARCSTATE, &marcstate, 1);
    } while (marcstate != 0x41);

    // 8) Read FS_VCO2, FS_VCO4 and FS_CHP register obtained
    //    with mid VCDAC_START value
    cc112xSpiReadReg(CC112X_FS_VCO2,
                     &calResults_for_vcdac_start_mid[FS_VCO2_INDEX], 1);
    cc112xSpiReadReg(CC112X_FS_VCO4,
                     &calResults_for_vcdac_start_mid[FS_VCO4_INDEX], 1);
    cc112xSpiReadReg(CC112X_FS_CHP,
                     &calResults_for_vcdac_start_mid[FS_CHP_INDEX], 1);

    // 9) Write back highest FS_VCO2 and corresponding FS_VCO
    //    and FS_CHP result
    if (calResults_for_vcdac_start_high[FS_VCO2_INDEX] >
        calResults_for_vcdac_start_mid[FS_VCO2_INDEX]) {
        writeByte = calResults_for_vcdac_start_high[FS_VCO2_INDEX];
        cc112xSpiWriteReg(CC112X_FS_VCO2, &writeByte, 1);
        writeByte = calResults_for_vcdac_start_high[FS_VCO4_INDEX];
        cc112xSpiWriteReg(CC112X_FS_VCO4, &writeByte, 1);
        writeByte = calResults_for_vcdac_start_high[FS_CHP_INDEX];
        cc112xSpiWriteReg(CC112X_FS_CHP, &writeByte, 1);
    } else {
        writeByte = calResults_for_vcdac_start_mid[FS_VCO2_INDEX];
        cc112xSpiWriteReg(CC112X_FS_VCO2, &writeByte, 1);
        writeByte = calResults_for_vcdac_start_mid[FS_VCO4_INDEX];
        cc112xSpiWriteReg(CC112X_FS_VCO4, &writeByte, 1);
        writeByte = calResults_for_vcdac_start_mid[FS_CHP_INDEX];
        cc112xSpiWriteReg(CC112X_FS_CHP, &writeByte, 1);
    }
}

void registerConfigMSK() {

    uint8 writeByte;

    // Reset radio
    trxSpiCmdStrobe(CC112X_SRES);

    // Write registers to radio
    for(uint16 i = 0;
        i < (sizeof(mskSettings)/sizeof(registerSetting_t)); i++) {
        writeByte = mskSettings[i].data;
        cc112xSpiWriteReg(mskSettings[i].addr, &writeByte, 1);
    }
}

// Switches the radio into MSK mode
void init_radio_msk() {
    // Clear interrupts
    ioPinIntClear(IO_PIN_PORT_1, GPIO0);
    ioPinIntDisable(IO_PIN_PORT_1, GPIO0);
    ioPinIntClear(IO_PIN_PORT_1, GPIO2);
    ioPinIntDisable(IO_PIN_PORT_1, GPIO2);
    
    // Reset radio, write new registers
    registerConfigMSK();
    
    uint8 writeByte;

    // Application specific registers
    // FIFO_THR = 120
    // GPIO0 = TXFIFO_THR
    // GPIO2 = PKT_SYNC_RXTX
    writeByte = INFINITE_PACKET_LENGTH_MODE;
    cc112xSpiWriteReg(CC112X_PKT_CFG0, &writeByte, 1);
    writeByte = 0x78; cc112xSpiWriteReg(CC112X_FIFO_CFG, &writeByte, 1);
    writeByte = 0x02; cc112xSpiWriteReg(CC112X_IOCFG0,   &writeByte, 1);
    writeByte = 0x06; cc112xSpiWriteReg(CC112X_IOCFG2,   &writeByte, 1);

    bspLedSet(BSP_LED_ALL);

    // Calibrate the radio according to the errata note
    manualCalibration();

    // Connect ISR function to GPIO0
    ioPinIntRegister(IO_PIN_PORT_1, GPIO0, &txFifoBelowThresholdISR);

    // Interrupt on falling edge
    ioPinIntTypeSet(IO_PIN_PORT_1, GPIO0, IO_PIN_FALLING_EDGE);

    // Clear interrupt
    ioPinIntClear(IO_PIN_PORT_1, GPIO0);

    // Enable interrupt
    ioPinIntEnable(IO_PIN_PORT_1, GPIO0);

    // Connect ISR function to GPIO2
    ioPinIntRegister(IO_PIN_PORT_1, GPIO2, &packetSentISR);

    // Interrupt on falling edge
    ioPinIntTypeSet(IO_PIN_PORT_1, GPIO2, IO_PIN_FALLING_EDGE);

    // Clear interrupt
    ioPinIntClear(IO_PIN_PORT_1, GPIO2);

    // Enable interrupt
    ioPinIntEnable(IO_PIN_PORT_1, GPIO2);
}

void msk_send_packet( uint8_t *data, int length ) {
    
    int length_original = length;
    
    // Packets must be at least 256 bytes
    if( length < 256 ) 
        length = 256;
    
    // Create data packet
    txBuffer[0] = (uint8)(length >> 8);
    txBuffer[1] = (uint8)(length& 0x00FF);
    for (uint16 i = 2; i < length + 2; i++) {
        if( i-2 <= length_original ) 
            txBuffer[i] = data[i-2];
        else 
            txBuffer[i] = 0;
    }
    
    uint8 writeByte;
    writeByte = INFINITE_PACKET_LENGTH_MODE;
    cc112xSpiWriteReg(CC112X_PKT_CFG0, &writeByte, 1);
    pktFormat = INFINITE;

    // Update variables
    writeRemainingDataFlag = FALSE;
    bytesLeft = length + 2;
    pBufferIndex = txBuffer + FIFO_SIZE;
    fixedPacketLength = bytesLeft % (MAX_VARIABLE_LENGTH + 1);

    // Configure the PKT_LEN register
    writeByte = fixedPacketLength;
    cc112xSpiWriteReg(CC112X_PKT_LEN, &writeByte, 1);

    // Fill up the TX FIFO
    cc112xSpiWriteTxFifo(txBuffer, FIFO_SIZE);

    bytesLeft -= FIFO_SIZE;

    iterations = (bytesLeft / AVAILABLE_BYTES_IN_TX_FIFO);

    // Enter TX mode
    trxSpiCmdStrobe(CC112X_STX);

    // Clear isr flag on GPIO0
    ioPinIntClear(IO_PIN_PORT_1, GPIO0);

    // Enable interrupt on GPIO0
    ioPinIntEnable(IO_PIN_PORT_1, GPIO0);

    // Wait for packet to be sent
    while (!packetSent);
    packetSent = FALSE;
}

#pragma optimize=none
void waitUs(uint16 uSec) { // 5 cycles for calling

    // The least we can wait is 3 usec:
    // ~1 usec for call, 1 for first compare and 1 for return
    while(uSec > 3) {  // 2 cycles for compare
                       // 2 cycles for jump
        NOP();         // 1 cycle for nop
        NOP();         // 1 cycle for nop
        NOP();         // 1 cycle for nop
        NOP();         // 1 cycle for nop
        NOP();         // 1 cycle for nop
        NOP();         // 1 cycle for nop
        NOP();         // 1 cycle for nop
        NOP();         // 1 cycle for nop
        uSec -= 2;     // 1 cycle for optimized decrement
    }
}                      // 4 cycles for returning


#pragma optimize=none
void waitMs(uint16 mSec) {
    while(mSec-- > 0) {
        waitUs(1000);
    }
}